const http = require('http');

const PORT = 4000;



http.createServer(function(request, response){
	if (request.url === "/"){
//a. If the url is http://localhost:400/, send a response Welcome to Booking System

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to Booking System')
	}

//b. If the url is http://localhost:400/profile, send a response Welcome to your profile

	else if(request.url == "/profile" && request.method === "GET" ){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to your profile')
	}
//c. If the url is http://localhost:400/courses, send a response Here's our courses available

	else if(request.url == "/courses" && request.method === "GET" ){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Here's our courses available")
	}
//d. If the url is http://localhost:400/addcourse, send a response Add course to our resources

	else if(request.url == "/addcourse" && request.method === "POST" ){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Add course to our resources")
	}
 //e. If the url is http://localhost:400/updatecourse, send a response Update a course to our resources

 else if(request.url == "/updatecourse" && request.method === "PUT" ){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Update a course to our resources')
	}
//f. If the url is http://localhost:400/archiveCourse, send a response Archive courses to our resources

	else if(request.url == "/archiveCourse" && request.method === "DELETE" ){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Archive courses to our resources')
	}


}).listen(PORT)

console.log(`Server is running at localhost:${PORT}`)